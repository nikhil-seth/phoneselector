package `in`.zeta.apollo.phoneselector

import android.app.PendingIntent
import android.content.Intent
import android.content.IntentSender
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.credentials.*


class MainActivity : AppCompatActivity() {
    val CREDENTIAL_PICKER_REQUEST = 1001
    private lateinit var editText:EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        editText = findViewById(R.id.editText)
        requestHint()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == CREDENTIAL_PICKER_REQUEST){
            when(resultCode) {
                RESULT_OK -> {
                    // We get the response.
                    val credentials = data?.getParcelableExtra<Parcelable>(Credential.EXTRA_KEY) as Credential
                    // accounttype,name,famName,idTokens,id,password,profilePictureUri
                    credentials .id.let { Log.d("TAG", it.toString()) }
                    credentials .idTokens.let { for(idx in it){ Log.d("TAGx", idx.toString())} }
                    editText.setText(credentials.id)
                }
                RESULT_CANCELED ->
                    Log.d("TAG", "onActivityResult: Cancelled by user")
                CredentialsApi.ACTIVITY_RESULT_ADD_ACCOUNT ->
                    Log.d("TAG", "onActivityResult: Add Account, only for accountType," +
                            " needs to be set in credentialPickerConfig")
                CredentialsApi.ACTIVITY_RESULT_NO_HINTS_AVAILABLE ->
                    Log.d("TAG", "onActivityResult: No Phone Numbers available, so we can resort to normal text")
            }
        }
    }
    fun requestHint(){
        /**
         * Using credentialPickerConfig, we can change prompt text. ( Predefined )
         * Also, we can remove/add cancel button.
         */
        val credentialPickerConfig = CredentialPickerConfig.Builder()
                //.setPrompt(CredentialPickerConfig.Prompt.SIGN_UP) / SignIn / Continue With
                //.setShowCancelButton(false) / Cancel Button
                //.setShowAddAccountButton(true) // Shows add account button.
                .build()

        /**
         * Emails, Ph. No, accounttypes can be taken
         */
        val hintRequest: HintRequest = HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .setHintPickerConfig(credentialPickerConfig)
                .build()
        val pd: PendingIntent = Credentials.getClient(this).getHintPickerIntent(hintRequest)
        try {
            startIntentSenderForResult(pd.intentSender, CREDENTIAL_PICKER_REQUEST, null, 0, 0,
                    0)
        }
        catch (e: IntentSender.SendIntentException){
            e.printStackTrace()
        }
    }
}